package com.msk.manisha.wikiapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.google.gson.Gson;
import com.msk.manisha.wikiapp.adapters.WikiAdapter;
import com.msk.manisha.wikiapp.models.Model;
import com.msk.manisha.wikiapp.models.Page;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
//Developed by manisha

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    WikiAdapter adapter;
    ProgressBar progressBar;
    Toolbar toolbar;
    private List<Page> pageslist = new ArrayList<Page>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progressBar=(ProgressBar)findViewById(R.id.progressbar);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        getData("Sachin+T");
        final android.support.v7.widget.SearchView et= (android.support.v7.widget.SearchView)findViewById(R.id.search);
                et.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        pageslist.clear();
                        progressBar.setVisibility(View.VISIBLE);
                        getData(query);
                        return false;
                    }
                    @Override
                    public boolean onQueryTextChange(String newText) {
                        progressBar.setVisibility(View.VISIBLE);
                        return false;
                    }
                });
            }

    private void getData(String gpssearch)
    {

        Retrofit r = new Retrofit.Builder()
                .baseUrl("https://en.wikipedia.org//")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

     String generator="prefixsearch";
     String redirects="1";
     String formatversion="2";
     String piprop="thumbnail";
     String  pithumbsize="150";
     String pilimit="10";
     String wbptterms="description";
     String gpslimit="10";
        APIInterface api = r.create(APIInterface.class);
        Call<Model> call = api.getResponse(generator,redirects,formatversion,
                piprop,pithumbsize,pilimit,wbptterms,
                gpssearch,gpslimit);
        call.enqueue(new retrofit2.Callback<Model>() {
            @Override
            public void onResponse(Call<Model> call, retrofit2.Response<Model> response) {
                Model wiki = response.body();
                  progressBar.setVisibility(View.GONE);
                Log.e("SignUp", new Gson().toJson(response.body()));
                try {
                    if (((wiki != null)) && (!wiki.getQuery().getPages().isEmpty()) && (wiki.getQuery() != null)) {
                      //  Toast.makeText(MainActivity.this, "size:" + wiki.getQuery().getPages().size(), Toast.LENGTH_SHORT).show();

                        pageslist.addAll(wiki.getQuery().getPages());
                        adapter = new WikiAdapter(MainActivity.this, pageslist);
                        recyclerView.setAdapter(adapter);
                    }
                }
                catch (NullPointerException e){
                    Toast.makeText(MainActivity.this, "Please enter valid keyword" , Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<Model> call, Throwable t) {
                  progressBar.setVisibility(View.GONE);
                Log.e("Why FAILURE SIGN UP",t.getMessage());
                Toast.makeText(MainActivity.this, "Check you internet connection", Toast.LENGTH_LONG).show();
            }
        });
    }

}

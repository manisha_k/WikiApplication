package com.msk.manisha.wikiapp.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.msk.manisha.wikiapp.MainActivity;
import com.msk.manisha.wikiapp.R;
import com.msk.manisha.wikiapp.models.Page;

import java.util.List;

public class WikiAdapter extends RecyclerView.Adapter<WikiAdapter.WikiHolder> {
    private List<Page> detail;
    Context mcontext;
    public WikiAdapter(MainActivity context, List<Page> detail) {
        this.mcontext = context;
        this.detail = detail;
    }

    @NonNull
    @Override
    public WikiHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view,parent, false);
        return new WikiHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull WikiHolder holder, final int position) {
        try {
            holder.title.setText(detail.get(position).getTitle());
            holder.desc.setText(detail.get(position).getTerms().getDescription().get(0));
            Glide.with(mcontext)
                    .load(detail.get(position).getThumbnail().getSource())
                    .asBitmap()
                    .placeholder(R.drawable.progress_two_v)
                    .centerCrop()
                    .into(holder.iv);
        }
        catch (NullPointerException e){
        }
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
           String url="https://en.wikipedia.org/wiki/"+detail.get(position).getTitle();
           openWebView(url,detail.get(position).getTitle());


            }
        });
    }
    public void openWebView(String url, String title)
    {

        final AlertDialog.Builder alert = new AlertDialog.Builder(mcontext);
        alert.setTitle(title);
        LayoutInflater inflater = LayoutInflater.from(mcontext);
        View dialogView = inflater.inflate(R.layout.webview_dialog, null);
        alert.setView(dialogView);

        final WebView myWebView = (WebView)dialogView.findViewById(R.id.webview);
        final ProgressBar myPB = (ProgressBar)dialogView.findViewById(R.id.progressbar);
        myWebView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {

                myWebView.setVisibility(View.INVISIBLE);
                myPB.setVisibility(View.VISIBLE);
                super.onPageStarted(view, url, favicon);

            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                view.loadUrl(url);
                return true;
            }
            @Override
            public void onPageFinished(WebView view, String url){
                myWebView.setVisibility(View.VISIBLE);
                myPB.setVisibility(View.GONE);
            }
        });
        myWebView.loadUrl(url);
        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        alert.show();


    }
    @Override
    public int getItemCount() {
        try {
            return detail.size();

        }
        catch (WindowManager.BadTokenException e){
            return Integer.parseInt(null);
        }
    }
    class WikiHolder extends RecyclerView.ViewHolder{
TextView title, desc;
ImageView iv;
LinearLayout linearLayout;
        public WikiHolder(View itemView) {
            super(itemView);
          title=itemView.findViewById(R.id.title);
          desc=itemView.findViewById(R.id.desc);
          iv=itemView.findViewById(R.id.iv);
          linearLayout=itemView.findViewById(R.id.clickable_item);
        }
    }
}
